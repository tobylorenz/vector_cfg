#include "File.h"

namespace Vector {
namespace CFG {

bool File::getObject(std::vector<std::string> path, std::list<std::string> & lines)
{
    unsigned int lineNumber = 0;

    /* clear output lines */
    lines.clear();

    /* argument check */
    if (path.empty()) {
        return false;
    }

    unsigned int depth = 0;
    std::string searchTerm = "";
    std::string objectName;
    unsigned int repetitions = 0;
    bool getNext = true;
    bool withinObject = false;

    /* seek to beginning of stream */
    seekg(0);

    /* read file line per line */
    std::string line;
    std::getline(*this, line); ++lineNumber;
    while (good()) {
        /* search the next depth */
        if (getNext) {
            if (repetitions > 0) {
                /* just repeat the same search term */
                --repetitions;
            } else {
                if (depth < path.size()) {
                    /* separate objectName from objectCount */
                    objectName = path[depth];
                    size_t hashPos = objectName.find_last_of('#');
                    if (hashPos != std::string::npos) {
                        repetitions = std::stoul(objectName.substr(hashPos + 1));
                        objectName.erase(hashPos);
                    } else {
                        repetitions = 0;
                    }
                    ++depth;

                    /* define begin of object */
                    searchTerm.clear();
                    searchTerm.append(objectName);
                    searchTerm.append(" ");
                    searchTerm.append(std::to_string(depth));
                    searchTerm.append(" ");
                    searchTerm.append("Begin_Of_Object");
                } else {
                    /* define end of object */
                    searchTerm.clear();
                    searchTerm.append("End_Of_Object");
                    searchTerm.append(" ");
                    searchTerm.append(objectName);
                    searchTerm.append(" ");
                    searchTerm.append(std::to_string(depth));
                }
            }

            getNext = false;
        }

        /* check whether the line contains the search term */
        if (line == searchTerm) {
            /* the wanted object ends here */
            if (withinObject == true) {
                return true;
            }

            /* the wanted object starts here */
            if ((depth >= path.size()) && (repetitions <= 0) ) {
                withinObject = true;
            }

            /* get the next search term */
            getNext = true;
        } else
        if (withinObject) {
            lines.push_back(line);
        }

        /* get next line */
        std::getline(*this, line); ++lineNumber;
    }

    return false;
}

}
}
