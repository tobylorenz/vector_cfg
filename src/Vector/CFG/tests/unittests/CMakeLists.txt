include_directories(${PROJECT_SOURCE_DIR}/src)

find_package(Boost 1.54 COMPONENTS system filesystem unit_test_framework REQUIRED)
include_directories(${Boost_INCLUDE_DIR})
link_directories(${Boost_LIBRARY_DIRS})

add_definitions(
  -DCMAKE_CURRENT_SOURCE_DIR="${CMAKE_CURRENT_SOURCE_DIR}"
  -DCMAKE_CURRENT_BINARY_DIR="${CMAKE_CURRENT_BINARY_DIR}")

add_executable(test_EnvironmentVariables test_EnvironmentVariables.cpp)

if(OPTION_USE_GCOV)
  add_definitions(-g -O0 -fprofile-arcs -ftest-coverage)
  target_link_libraries(test_EnvironmentVariables cov)
endif()

target_link_libraries(test_EnvironmentVariables
  Vector_CFG
  ${Boost_SYSTEM_LIBRARY}
  ${Boost_FILESYSTEM_LIBRARY}
  ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY})

enable_testing()

add_test(Database test_EnvironmentVariables)
