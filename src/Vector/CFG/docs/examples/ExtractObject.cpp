#include <iostream>
#include <list>
#include <string>
#include <vector>

#include "Vector/CFG/File.h"

int main(int argc, char *argv[])
{
    if (argc <= 2) {
        std::cout << "ExtractObject <filename.cfg> {path elements}" << std::endl;
        std::cout << "EnvironmentVariables: VGlobalConfiguration VGlobalParameters VSVConfigurationStreamer" << std::endl;
        return 0;
    }

    Vector::CFG::File file;
    file.open(argv[1]);
    if (!file.is_open()) {
        std::cout << "Unable to open file" << std::endl;
        return 0;
    }

    /* set path */
    std::vector<std::string> path;
    for (int i = 2; i < argc; ++i) {
        path.push_back(argv[i]);
    }

    /* get lines */
    std::list<std::string> lines;
    file.getObject(path, lines);

    /* show lines */
    for (auto line : lines) {
        std::cout << line << std::endl;
    }

    return 0;
}
