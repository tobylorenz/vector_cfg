#pragma once

#include <fstream>
#include <list>
#include <string>
#include <vector>

namespace Vector {
namespace CFG {

class File : public std::ifstream
{
public:
    /**
     * Get the lines for the object given in path
     *
     * @param[in] path
     *   This is the locator path. Each string can have a suffix #<nr> to mention the (n-1)th element.
     *
     * @param[out] lines
     *   This gives back the lines given in path.
     *
     * @return
     *   True, if successful.
     */
    bool getObject(std::vector<std::string> path, std::list<std::string> & lines);
};

}
}
