cmake_minimum_required(VERSION 2.8)

include(GenerateExportHeader)

configure_file(
  config.h.in
  config.h)

include_directories(${CMAKE_CURRENT_BINARY_DIR})

set(source_files
  File.cpp)

set(header_files
  File.h)

add_compiler_export_flags()

add_library(${PROJECT_NAME} SHARED ${source_files} ${header_files})

if(OPTION_USE_GCOV_LCOV)
  target_link_libraries(${PROJECT_NAME} gcov)
  add_definitions(-g -O0 -fprofile-arcs -ftest-coverage)
endif()

generate_export_header(${PROJECT_NAME})

if(CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-std=c++11)
endif()

install(
  TARGETS ${PROJECT_NAME}
  DESTINATION lib)

install(
  FILES ${CMAKE_CURRENT_BINARY_DIR}/vector_cfg_export.h
  DESTINATION include/Vector/CFG)

install(
  FILES ${CMAKE_CURRENT_BINARY_DIR}/config.h ${header_files}
  DESTINATION include/Vector/CFG)

add_subdirectory(docs)

add_subdirectory(tests)
